/**
 * naivly calculating pi, saving each intermediate step into a file, running indefinetly
 */

#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <math.h>

typedef int FD;

int main(int argc, char const *argv[])
{

    FD output_sink = open("/tmp/rr-output.sock", O_RDWR | O_APPEND);
    if (!output_sink)
    {
        printf("could not open output file\n");
        exit(EXIT_FAILURE);
    }

    double pi_quarters = 0.0;
    double step = 0.0;

    while (1)
    {
        pi_quarters = pi_quarters + ((pow(-1.0, step))/(2.0 * step + 1.0));
        
        printf("%1.20f\n", pi_quarters * 4.0);
        char buf[16];
        sprintf(buf, "%1.20f\n", pi_quarters * 4);
        write(output_sink, buf, sizeof(buf));
        step++;
    }
    return 0;
}
