# Compiler
CC = clang

# flags
CFLAGS = -Wall

SRC = src/main.c
TARGET = rr-dummy-program

all: $(TARGET)

$(TARGET): $(SRC)
	$(CC) $(CFLAGS) -o $(TARGET) $(SRC) -lm

clean:
	rm rr-dummy-program